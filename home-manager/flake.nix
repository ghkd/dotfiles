{
  description = "My Home Manager configuration for Turing Pi 2 nodes";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, ... }@inputs:
    let
      mkHomeConfig = { username, system, baseModules ? [
        ./home.nix
        {
          home = {
            inherit username;
            homeDirectory = "/home/${username}";
          };
        }
      ], extraModules ? [ ], }:
        home-manager.lib.homeManagerConfiguration rec {
          pkgs = import nixpkgs { inherit system; };
          extraSpecialArgs = { inherit self inputs nixpkgs; };
          modules = baseModules ++ extraModules;
        };

      turingpi = {
        attrs = {
          username = "guilherme";
          system = "aarch64-linux";
        };

        nodes = [ "echo" "sierra" "november" "whiskey" ];
      };
    in {
      homeConfigurations = builtins.listToAttrs (builtins.map (n: {
        name = "${turingpi.attrs.username}@${n}";
        value = mkHomeConfig turingpi.attrs;
      }) turingpi.nodes);
    };
}
